﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public GameObject armadillo;

    private Vector3 banana;

	// Use this for initialization
	void Start ()
    {
        banana = transform.position - armadillo.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = armadillo.transform.position + banana;
	}
}
