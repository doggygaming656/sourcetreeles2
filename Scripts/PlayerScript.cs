﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    public Text countText;
    public Text winText;
    public Text loseText;

    private float speed = 20;
    private Rigidbody rigid;
    private int count;

	// Use this for initialization
	void Start ()
    {
        rigid = GetComponent<Rigidbody>();
        count = 0;
        setcounttext();
        winText.text = "";
        loseText.text = "";
	}
	
	// Update is called once per frame
	void Update ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rigid.AddForce(movement * speed);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("drugs"))
        {
            other.gameObject.SetActive(false);
            count = count + 10;
            setcounttext();
        }
    }

    void setcounttext()
    {
        countText.text = "räkna: " + count.ToString();
        if (count >= 120)
        {
            SceneManager.LoadScene("armadillo ball lv2");
        }
    }
}
